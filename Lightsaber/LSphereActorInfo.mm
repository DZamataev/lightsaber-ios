//
//  LSphereActorInfo.mm
//  Lightsaber
//
//  Created by Denis on 4/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LSphereActorInfo.h"
#import "LContactHolder.h"
#import "ProjectileActorInfo.h"

@implementation LSphereActorInfo
@synthesize type = _type;
@synthesize health = _health;
@synthesize collisionDamageMod = _collisionDamageMod;
@synthesize burnDamageMod = _burnDamageMod;
@synthesize projectileDamageMod = _projectileDamageMod;
@synthesize collisionCirclePadding = _collisionCirclePadding;
@synthesize mass = _mass;
@synthesize linearDamping = _linearDamping;
@synthesize angularDamping = _angularDamping;
@synthesize colDensity = _colDensity;
@synthesize colFriction = _colFriction;
@synthesize colRestitution = _colRestitution;
@synthesize burnContact = _burnContact;
@synthesize beamCollisionDamage = _beamCollisionDamage;
@synthesize projectileCollisionPool = _projectileCollisionPool;
-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att
{
    self = [super initWithPosition:pos andRotation:rotation andRect:rect andGotType:got andAttType:att];
    _type = LSTSingle;
    _health = 100;
    _collisionCirclePadding = 7;
    _burnDamageMod = 1;
    _projectileDamageMod = 1;
    _collisionDamageMod = 1;
    _mass = 0.01f;
    _linearDamping = 0;
    _angularDamping = 0;
    _colDensity = 0;
    _colFriction = 0;
    _colRestitution = 0.1f;
    _burnContact = 0;
    _beamCollisionDamage = 0;
    _projectileCollisionPool = [NSMutableArray new];
    return self;
}

-(void)pushContactedObjectInfoProjectileCollisionPool:(GameActorInfo*)objInfo
{
    for (LContactHolder *h in _projectileCollisionPool)
    {
        if ([h.contactedObjectInfo isEqual:objInfo])
        {
            return;
        }
    }
    
    LContactHolder *newHolder = [LContactHolder LCHolderWithContactedObjectInfo:objInfo];
    [_projectileCollisionPool addObject:newHolder];
    // deal damage
    if (objInfo.attType == attProjectile)
    {
        ProjectileActorInfo *projInfo = (ProjectileActorInfo*)objInfo;
        [self dealDamage:projInfo.damage withModifier:_projectileDamageMod];
    }

}

-(void)dealDamage:(float)dmg withModifier:(float)mod
{
    self.health -= dmg * mod;
}

-(void)dealloc
{
    self.projectileCollisionPool = nil;
    [super dealloc];
}
@end
