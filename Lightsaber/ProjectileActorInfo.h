//
//  ProjectileActorInfo.h
//  Shooter
//
//  Created by Denis on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameActorInfo.h"
#import "Box2D.h"

#define ENABLE_TINT_ON_CHANGE_CONTACT_STATE

typedef enum {pcsEnemy,pcsSaber}ProjectileContactState;

@interface ProjectileActorInfo : GameActorInfo
{
    GameActorInfo *_collidedObject;
    int _damage;
    bool _impulseApplied;
    bool _constantSpeed;
    int _bounces;
    int _penetration;
    NSMutableArray *_penetrated;
    NSMutableArray *_contacted;
    float _maximumSpeed;
    ProjectileContactState _contactState;
}

@property (nonatomic, assign) GameActorInfo *collidedObject;
@property int damage;
@property bool impulseApplied;
@property int bounces;
@property int penetration;
@property bool constantSpeed;
@property float maximumSpeed;
@property ProjectileContactState contactState;
-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att;

-(void)pushPenetratedInfo:(GameActorInfo*)info;
-(void)pushContact:(GameActorInfo*)info;
-(bool)actorAlreadyPenetrated:(GameActorInfo*)info;
-(bool)actorAlreadyContacted:(GameActorInfo*)info;
-(void)eraseContacted:(GameActorInfo*)info;
@end
