//
//  SingleLightsaber.mm
//  Lightsaber
//
//  Created by Denis on 3/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "SingleLightsaber.h"
#import "SHb2dCollisionManager.h"
#import "LPlume.h"
#import "LContactHolder.h"

@implementation SingleLightsaber
@synthesize myInfo=_myInfo;
@synthesize touchEnabled = _touchEnabled;
@synthesize touchLocation = _touchLocation;
@synthesize handle = _handle;
@synthesize beam = _beam;

-(id)initOnLayer:(CCLayer*)lay
{
    self = [super init];
    // zeroing
    _touchEnabled = YES;
    _touchLocation = ccp(0,0);
    _isDualTouched = NO;
    _beam = [CCSprite spriteWithFile:@"Saber/Single/kyle/beam.png"];
    _handle = [CCSprite spriteWithFile:@"Saber/Single/kyle/handle.png"];
    _beam.scale = 1.5f;
    _handle.scale = 1.5f;
    _initialHandleSize = _handle.boundingBox.size;
    _beam.anchorPoint = ccp(0.5f,0);
    _handle.anchorPoint = ccp(0.5f,0);
    _handlePoint=ccp(0,0);
    [self addChild:_beam];
    [self addChild:_handle];
    _beam.position = ccp(_beam.position.x, _handle.boundingBox.size.height);
    
//    _radius = _handle.boundingBox.size.height*20.0f;
     _radius = 200.0f;
    _layer = lay;
    
    
    _plume = nil;
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self setupPhysicsBody];
    [self scheduleUpdate];
}


-(void)setupPhysicsBody
{
    _myInfo = [[SingleLightsaberActorInfo alloc] initWithPosition:ccpAdd(self.position, ccp(0,_handle.boundingBox.size.height/2)) andRotation:0 andRect:_handle.boundingBox andGotType:gotDynamic andAttType:attSaber andBeamRect:_beam.boundingBox andHandleRect:_handle.boundingBox];
    [[SHb2dCollisionManager sharedSHb2dCollisionManager] addSingleLightsaberBodyWithInfo:_myInfo];
    
    b2Body *sBody = [SHb2dCollisionManager sharedSHb2dCollisionManager].saberBody;
    struct b2MassData md = [SHb2dCollisionManager sharedSHb2dCollisionManager].saberWithHandleMassData;
    sBody->SetMassData(&md);

}

-(void)update:(ccTime)dt
{
    _previousBeamCenterPosition = _currentBeamCenterPosition;

    
    
    b2Body *sBody = [SHb2dCollisionManager sharedSHb2dCollisionManager].saberBody;
    b2Vec2 zeroVec = sBody->GetWorldPoint(b2Vec2(0.0f,-_handle.boundingBox.size.height/2/PTM_RATIO));
    CGPoint zeroPos = ccp(zeroVec.x*PTM_RATIO,zeroVec.y*PTM_RATIO);
    b2Vec2 handleVec = sBody->GetWorldPoint(b2Vec2(_myInfo.anchorOfBodyToHandle.x,_myInfo.anchorOfBodyToHandle.y));
    _handlePoint = ccp(handleVec.x *PTM_RATIO,handleVec.y *PTM_RATIO);
    self.position = zeroPos;
    self.rotation = _myInfo.rotation;
    
    _currentBeamCenterPosition = [self convertToWorldSpaceAR:ccp(_beam.position.x,_beam.position.y+_beam.boundingBox.size.height/2)];
    // first Update should init the plume
//    if (_plume == nil)
//    {
//        MotionStreakInitData streakData;
//        streakData.color = ccc3(255, 255, 255);
//        streakData.fade = 0.10;
//        streakData.minSeg = 0.001;
//        streakData.width = _beam.boundingBox.size.width/10;
//        streakData.texture = [[CCTextureCache sharedTextureCache] addImage:@"Saber/Single/kyle/plume.png"];
//        
//        _plume = [[LPlume alloc] initWithBeamSize:CGSizeMake(_beam.boundingBox.size.width/20, _beam.boundingBox.size.height) andCenterPoint:[self convertToWorldSpace:ccp(_beam.position.x,_beam.position.y+_beam.boundingBox.size.height/2)] andSaberNode:self andMotionStreakInitData:streakData onLayer:_layer withZIndex:self.zOrder-1];
//        [_plume updateStreaks];
//    }
//    else {
//        if (ccpFuzzyEqual(_previousBeamCenterPosition, _currentBeamCenterPosition, 5))
//        {
//            [_plume interruptStreaks];
////            if (ccpFuzzyEqual(_previousBeamCenterPosition, _currentBeamCenterPosition, 0.1))
////            {
////                [_plume interruptStreaks];
////            }
//        }
//        else
//        {
//             
//            [_plume updateStreaks];
//        }
//    }
    
    // update handle contacts
    for (int i = 0; i<[_myInfo.handleContactsPool count];)
    {
        LContactHolder *h = [_myInfo.handleContactsPool objectAtIndex:i];
        h.lifeTimer -= dt;
        if (h.lifeTimer < 0)
        {
            [_myInfo.handleContactsPool removeObjectAtIndex:i];
        }
        else {
            i++;
        }
    }
    
}

-(void)onTouchBegin
{
    if ([SHb2dCollisionManager sharedSHb2dCollisionManager].saberBody == NULL || [SHb2dCollisionManager sharedSHb2dCollisionManager].saberBody == nil)
    {
        NSLog(@"gush");
    }
    //lets work on touches
    if (_mouseJoint != NULL) return;
    CGPoint location = _touchLocation;
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    b2MouseJointDef md;
    md.bodyA = [SHb2dCollisionManager sharedSHb2dCollisionManager].mapBoundsBody;
    md.bodyB = [SHb2dCollisionManager sharedSHb2dCollisionManager].saberBody;
    b2Vec2 anchor;
    anchor.x = _myInfo.anchorOfBodyToHandle.x;
    anchor.y = _myInfo.anchorOfBodyToHandle.y-_handle.boundingBox.size.height/2/PTM_RATIO;
    b2Vec2 target = [SHb2dCollisionManager sharedSHb2dCollisionManager].saberBody->GetWorldPoint(anchor);
    md.target = target;
    md.collideConnected = true;
    md.maxForce = _myInfo.dragForceMultiplier * [SHb2dCollisionManager sharedSHb2dCollisionManager].saberBody->GetMass();
    b2World *world_ = [SHb2dCollisionManager sharedSHb2dCollisionManager].world;
    _mouseJoint = (b2MouseJoint *)world_->CreateJoint(&md);
}

-(void)onTouchMoved
{
    if (_mouseJoint == NULL) return;
    
    CGPoint location = _touchLocation;
    
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    
    _mouseJoint->SetTarget(locationWorld);
}

-(void)onTouchEnded
{
    if (_mouseJoint) {
        [SHb2dCollisionManager sharedSHb2dCollisionManager].world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }  
}

-(void)onTouchCancelled
{
    if (_mouseJoint) {
        [SHb2dCollisionManager sharedSHb2dCollisionManager].world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
}

-(bool)positionIsWithinRadius:(CGPoint)pos
{
    return fabsf(ccpDistance(_handlePoint, pos)) <= _radius;
}

-(void)setIsDualTouched:(bool)isDualTouched
{
    _isDualTouched = isDualTouched;
    if (isDualTouched)
    {
        b2Body *sBody = [SHb2dCollisionManager sharedSHb2dCollisionManager].saberBody;
        sBody->GetMassData(&_savedForDoubletouchMassData);
        _savedForDoubletouchI = _savedForDoubletouchMassData.I;
        _savedForDoubletouchMassData.I = 0;
        sBody->SetMassData(&_savedForDoubletouchMassData);
    }
    else {
        b2Body *sBody = [SHb2dCollisionManager sharedSHb2dCollisionManager].saberBody;
        _savedForDoubletouchMassData.I = _savedForDoubletouchI;
        sBody->SetMassData(&_savedForDoubletouchMassData);
    }
}

-(bool)isDualTouched
{
    return _isDualTouched;
}

-(CGPoint)getTargetPosition
{
    return [self convertToWorldSpace:ccp(_handle.position.x,_handle.position.y+_handle.boundingBox.size.height/2)];
}

-(void)dealloc
{
    [_plume release];
    [_myInfo release];
    [super dealloc];
}
@end
