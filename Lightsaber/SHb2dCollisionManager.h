//
//  SHb2dCollisionManager.h
//  Shooter
//
//  Created by Denis on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWLSynthesizeSingleton.h"
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "MyContactListener.h"

#import "GameActorInfo.h"

#define PTM_RATIO 32

#define CATEGORY_BOUNDRY 0x0001
#define CATEGORY_SABER_HANDLE 0x0002
#define CATEGORY_SABER_BEAM 0x0004
#define CATEGORY_SPHERE 0x0008
#define CATEGORU_PROJECTILE 0x0016
#define CATEGORY_SPHERE_SENSOR 0x0032

struct SHBodyInitType {
    b2BodyType btype;
    bool isSensor;
    bool isPlayer;
    float linearDamping;
    float angularDamping;
    float friction;
    float restitution;
    bool isBullet;
    uint32 categoryBits;
    uint32 maskBits;
    bool customMass;
    float mass;
};
@interface SHb2dCollisionManager : NSObject
{
    b2World *_world;
    b2Body *_mapBoundsBody;
    GLESDebugDraw *_debugDraw;
    MyContactListener *_contactListener;
    NSMutableArray *_players;
    b2Body *_saberBody;
    b2MassData _saberWithHandleMassData;
}
@property (nonatomic,assign) b2World *world;
@property (nonatomic,assign) b2Body *mapBoundsBody;
@property (nonatomic,assign) b2Body *saberBody;
@property (nonatomic,assign) b2MassData saberWithHandleMassData;
CWL_DECLARE_SINGLETON_FOR_CLASS(SHb2dCollisionManager)

-(void) createMapBounds:(CGSize)size;
-(void)tick:(ccTime)dt;
-(void)drawDebug;
-(void)addBoxBodyWithCircleShapeForGameObjectWithInfo:(GameActorInfo *)info;
-(void)addBoxBodyWithPolygonShapeForGameObjectWithInfo:(GameActorInfo *)info;
-(void)addSingleLightsaberBodyWithInfo:(GameActorInfo*)info;
-(void)addSphereBodyWithInfo:(GameActorInfo*)info;

-(void)manageContactWithActorInfo:(GameActorInfo*)infoA andActorInfo:(GameActorInfo*)infoB;
-(void)manageContactWithDynamicActorInfo:(GameActorInfo*)dynInfo 
                      andStaticActorInfo:(GameActorInfo*)staticInfo;
-(void)manageContactWithDynamicActorInfo:(GameActorInfo*)dynInfoA 
                      andDynamicActorInfo:(GameActorInfo*)dynInfoB;

// shooter spectific methods

-(SHBodyInitType)switchInitTypeWithGOT:(gameObjectType)got andATT:(attachedToType)att;

// Non box2d collision tests

-(bool)rect:(CGRect)rect intersectsCircleWithOrigin:(CGPoint)p1 andRadius:(CGFloat)rect;
-(CGRect)calculateRectForPosition:(CGPoint)position andSize:(CGSize)size;
-(bool)rectCollisionTestForActorWithInfo:(GameActorInfo*)info1 andActorWithInfo:(GameActorInfo*)info2;

@end
