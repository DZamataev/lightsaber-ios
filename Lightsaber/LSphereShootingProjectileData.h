//
//  LSphereShootingProjectileData.h
//  Lightsaber
//
//  Created by Denis on 4/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Projectile.h"

@interface LSphereShootingProjectileData : NSObject {
    
}
@property float angle;
@property float delay;
@property float speedMod;
@property projectileType type;
@property float damage;
@property int firePortNum;
-(id)init;
@end 
