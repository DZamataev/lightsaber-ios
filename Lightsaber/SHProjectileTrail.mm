//
//  SHProjectileTrail.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHProjectileTrail.h"

@implementation SHProjectileTrail
@synthesize motionStreak = _motionStreak;
+(id)trailNodeWithMotionStreakWithFade:(float)f minSeg:(float)m width:(float)w textureFilename:(NSString *)t onLayer:(CCLayer*)l withPosition:(CGPoint)p; 
{
    return [[[SHProjectileTrail alloc] initWithMotionStreakWithFade:f minSeg:m width:w textureFilename:t onLayer:l withPosition:p] autorelease];
}
-(id)initWithMotionStreakWithFade:(float)f minSeg:(float)m width:(float)w textureFilename:(NSString *)t onLayer:(CCLayer*)l withPosition:(CGPoint)p;
{
    self = [super init];
    _destroyTimerToggle = NO;
    _timeAccum = 0;
    self.motionStreak = [CCMotionStreak streakWithFade:f minSeg:m width:w color:ccc3(227, 32, 12) textureFilename:t];
    self.motionStreak.position = p;
    [l addChild:self.motionStreak];
    return self;
}

-(void)update:(ccTime)dt
{
    // updates only just before destroy
    _timeAccum += dt;
    if (_timeAccum >= _destroyTime)
    {
        [self.motionStreak removeFromParentAndCleanup:YES];
        [self release];
    }
}
        
-(void)destroyInTimeUnattached:(float)t
{
    [self retain];
    [self scheduleUpdate];
    _destroyTime = t;
}

-(void)dealloc
{
    self.motionStreak = nil;
    [super dealloc];
}

@end
