//
//  PlayerShooting.h
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Projectile.h"

@class LSphere;
@class LSphereShootingProjectileData;



@interface LSphereShooting : CCNode
{
    CCLayer *_layer;
    float _timeAccum;
    float _nextShotTime;
    int _currentShotIndex;
    LSphere *_sphere;
    NSMutableArray *_firePorts;
    NSMutableArray *_shots;
}
-(id)initWithSphere:(LSphere*)sphere andLayer:(CCLayer *)layer andShootingDataDict:(NSDictionary*)shootingData;
-(void)tick:(float)dt;
@end
