//
//  ProjectileActorInfo.m
//  Shooter
//
//  Created by Denis on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProjectileActorInfo.h"
#import "Projectile.h"


@implementation ProjectileActorInfo
@synthesize collidedObject = _collidedObject;
@synthesize damage = _damage;
@synthesize impulseApplied = _impulseApplied;
@synthesize bounces = _bounces;
@synthesize penetration = _penetration;
@synthesize constantSpeed = _constantSpeed;
@synthesize maximumSpeed = _maximumSpeed;
-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att;
{
    self = [super initWithPosition:pos andRotation:rotation andRect:rect andGotType:got andAttType:att];
    _damage = 1;
    _constantSpeed = YES;
    _collidedObject = nil;
    _impulseApplied = NO;
    _penetration = 0;
    _bounces = 0;
    _maximumSpeed = 0;
    _penetrated = [NSMutableArray new];
    _contacted = [NSMutableArray new];
    _contactState = pcsEnemy;
    return self;
}

-(void)pushPenetratedInfo:(GameActorInfo *)info
{
    [_penetrated addObject:info];
}

-(bool)actorAlreadyPenetrated:(GameActorInfo *)info
{
    return [_penetrated containsObject:info];
}

-(void)pushContact:(GameActorInfo *)info
{
    [_contacted addObject:info];
}

-(bool)actorAlreadyContacted:(GameActorInfo *)info
{
    return [_contacted containsObject:info];
}

-(void)eraseContacted:(GameActorInfo *)info
{
//    NSLog(@"count before:%i",[_contacted count]);
    [_contacted removeObject:info];
//    NSLog(@"count after:%i",[_contacted count]);
}

-(ProjectileContactState)contactState
{
    return _contactState;
}

-(void)setContactState:(ProjectileContactState)contactState
{
    _contactState = contactState;
    
#ifdef ENABLE_TINT_ON_CHANGE_CONTACT_STATE
    
    CCSprite *spr = ((Projectile*)self.hostObject).sprite;
    CCTintBy *tint;
    switch (contactState) {
        case pcsEnemy:
//            tint = [CCTintBy actionWithDuration:0.1f red:120 green:120 blue:120];
//            [spr runAction:tint];
            break;
            
        case pcsSaber:
//            tint = [CCTintBy actionWithDuration:0.1f red:100 green:0 blue:100];
//            [spr runAction:tint];
            break;
            
        default:
            break;
    }
#endif
    
    Projectile* hostBodyAsDelegate = ((Projectile*)self.hostObject);
    [hostBodyAsDelegate onProjectileContactStateChange:contactState];
    
}



-(void)dealloc
{
    [_contacted release];
    [_penetrated release];
    _collidedObject = nil;
    [super dealloc];
}
@end
