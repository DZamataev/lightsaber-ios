//
//  SingleLightsaberActorInfo.h
//  Lightsaber
//
//  Created by Denis on 3/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameActorInfo.h"


@interface SingleLightsaberActorInfo : GameActorInfo
{
    CGPoint _angularVelocity;
    CGRect _beamRect;
    CGRect _handleRect;
    CGPoint _anchorOfBodyToHandle;
    float _dragForceMultiplier;
    NSMutableArray *_handleContactsPool; // will contain LContactHolder instances
}
@property CGPoint angularVelocity;
@property CGRect beamRect;
@property CGPoint anchorOfBodyToHandle;
@property float dragForceMultiplier;
@property (nonatomic, retain) NSMutableArray *handleContactsPool;
-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att andBeamRect:(CGRect)beamRect andHandleRect:(CGRect)handleRect;
-(void)pushContactedObjectInfoToHandleContactsPool:(GameActorInfo*)objInfo;
@end
