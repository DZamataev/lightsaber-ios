//
//  HUDLayer.h
//  Lightsaber
//
//  Created by Denis on 4/14/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class LUIHandlePointPicker;
@class SingleLightsaberActorInfo;
@class GameplayLayer;

// define the positions
#define HPP_BUTTON_PADDING 40.0f

@interface HUDLayer : CCLayer {
    LUIHandlePointPicker *_HPP;
    UITouch *_HPPTouch;
    GameplayLayer *_gameplay;
    CCMenu *_sideMenu;
    CCLabelTTF *_resetLabel;
    CCMenuItemLabel *_resetMenuItemLabel;
}
@property (nonatomic, retain) LUIHandlePointPicker *HPP;
@property (nonatomic, assign) GameplayLayer *gameplay;

-(void)addHandlePointPickerWithHandleSprite:(CCSprite*)handle andBeamSprite:(CCSprite*)beam andInfo:(SingleLightsaberActorInfo*)info;
-(void)resetButtonTouchInsideOut;
@end
