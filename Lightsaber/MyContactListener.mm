//
//  MyContactListener.m
//  Box2DPong
//
//  Created by Ray Wenderlich on 2/18/10.
//  Copyright 2010 Ray Wenderlich. All rights reserved.
//

#import "MyContactListener.h"
#import "GameActorInfo.h"
#import "ProjectileActorInfo.h"
#import "SHb2dCollisionManager.h"
#import "SingleLightsaberActorInfo.h"
#import "LSphereActorInfo.h"

MyContactListener::MyContactListener() : _contacts() {
}

MyContactListener::~MyContactListener() {
}

void MyContactListener::BeginContact(b2Contact* contact) {
    // We need to copy out the data because the b2Contact passed in
    // is reused.
    MyContact myContact = { contact->GetFixtureA(), contact->GetFixtureB() };
    _contacts.push_back(myContact);
}

void MyContactListener::EndContact(b2Contact* contact) {
    MyContact myContact = { contact->GetFixtureA(), contact->GetFixtureB() };
    std::vector<MyContact>::iterator pos;
    pos = std::find(_contacts.begin(), _contacts.end(), myContact);
    if (pos != _contacts.end()) {
        _contacts.erase(pos);
    }
}

void MyContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
    GameActorInfo *info1 = (GameActorInfo*)contact->GetFixtureA()->GetBody()->GetUserData();
    GameActorInfo *info2 = (GameActorInfo*)contact->GetFixtureB()->GetBody()->GetUserData();
    
    if (info1.attType == attSaber || info2.attType == attSaber)
    {
        b2Fixture *saberFixture;
        SingleLightsaberActorInfo *saberInfo;
        GameActorInfo *otherInfo;
        b2Fixture *otherFixture;
        if (info1.attType == attSaber) {
            saberFixture = contact->GetFixtureA();
            saberInfo = (SingleLightsaberActorInfo*)info1;
            otherInfo = info2;
            otherFixture = contact->GetFixtureB();
        }
        else if (info2.attType == attSaber) {
            saberFixture = contact->GetFixtureB();
            saberInfo = (SingleLightsaberActorInfo*)info2;
            otherInfo = info1;
            otherFixture = contact->GetFixtureA();
        }
        b2Filter filter = saberFixture->GetFilterData();
        if (otherInfo.attType == attProjectile)
        {
            ProjectileActorInfo *projInfo = (ProjectileActorInfo*)otherInfo;
            if (filter.categoryBits == CATEGORY_SABER_HANDLE)
            {
                if (projInfo.contactState == pcsEnemy) {
                    [saberInfo pushContactedObjectInfoToHandleContactsPool:projInfo];
                }
                else if (projInfo.contactState == pcsSaber) {
                    
                }
                contact->SetEnabled(FALSE);
            }
            else if (filter.categoryBits == CATEGORY_SABER_BEAM)
            {
                if (projInfo.contactState == pcsEnemy) {
                    projInfo.contactState = pcsSaber;
                }
            }
        }
        else if (otherInfo.attType == attSphere)
        {
            LSphereActorInfo *sphereInfo = (LSphereActorInfo*)otherInfo;
            b2Filter sphereFilter = otherFixture->GetFilterData();
            if (filter.categoryBits == CATEGORY_SABER_BEAM)
            {
                if (sphereFilter.categoryBits == CATEGORY_SPHERE_SENSOR)
                {
                    sphereInfo.burnContact = YES;
                    contact->SetEnabled(FALSE);
                }
//                else if (sphereFilter.categoryBits == CATEGORY_SPHERE)
//                {
//                    b2Vec2 normal = contact->GetManifold()->localNormal;
//                    NSLog(@"Norm x%f y%f l%f",normal.x,normal.y,normal.Length());
//                    
//                    
//                }
            }
        }
    }
    else if ((info1.attType == attProjectile && info2.attType == attSphere) || (info2.attType == attProjectile && info1.attType == attSphere))
    {
        LSphereActorInfo *sphereInfo;
        ProjectileActorInfo *projectileInfo;
        if (info1.attType == attSphere)
        {
            sphereInfo = (LSphereActorInfo*)info1;
            projectileInfo = (ProjectileActorInfo*)info2;
        }
        else {
            sphereInfo = (LSphereActorInfo*)info2;
            projectileInfo = (ProjectileActorInfo*)info1;
        }
        if (projectileInfo.contactState == pcsEnemy)
        {
            contact->SetEnabled(FALSE);
        }
        else if (projectileInfo.contactState == pcsSaber)
        {
            [sphereInfo pushContactedObjectInfoProjectileCollisionPool:projectileInfo];
        }
    }
}

void MyContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) {
    GameActorInfo *info1 = (GameActorInfo*)contact->GetFixtureA()->GetBody()->GetUserData();
    GameActorInfo *info2 = (GameActorInfo*)contact->GetFixtureB()->GetBody()->GetUserData();
    
    if (info1.attType == attSaber || info2.attType == attSaber)
    {
        b2Fixture *saberFixture;
        SingleLightsaberActorInfo *saberInfo;
        GameActorInfo *otherInfo;
        b2Fixture *otherFixture;
        int numberOfSphereImpulse = 0;
        if (info1.attType == attSaber) {
            saberFixture = contact->GetFixtureA();
            saberInfo = (SingleLightsaberActorInfo*)info1;
            otherInfo = info2;
            otherFixture = contact->GetFixtureB();
            numberOfSphereImpulse = 0;
        }
        else if (info2.attType == attSaber) {
            saberFixture = contact->GetFixtureB();
            saberInfo = (SingleLightsaberActorInfo*)info2;
            otherInfo = info1;
            otherFixture = contact->GetFixtureA();
            numberOfSphereImpulse = 0;
        }
        b2Filter filter = saberFixture->GetFilterData();
        if (otherInfo.attType == attSphere)
        {
            LSphereActorInfo *sphereInfo = (LSphereActorInfo*)otherInfo;
            b2Filter sphereFilter = otherFixture->GetFilterData();
            if (filter.categoryBits == CATEGORY_SABER_BEAM)
            {
                if (sphereFilter.categoryBits == CATEGORY_SPHERE)
                {
                    float sphereNormImp = impulse->normalImpulses[numberOfSphereImpulse];
//                    NSLog(@"NUM OF IMP %i lenght %f",numberOfSphereImpulse,sphereNormImp);
                    sphereInfo.beamCollisionDamage = sphereNormImp;
                    
                }
            }
        }
    }

}

