//
//  PlayerShooting.mm
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LSphereShooting.h"
#import "Projectile.h"
#import "LSphere.h"
#import "LSphereShootingProjectileData.h"
#import "SHb2dCollisionManager.h"

@implementation LSphereShooting
-(id)initWithSphere:(LSphere *)sphere andLayer:(CCLayer *)layer andShootingDataDict:(NSDictionary *)shootingData
{
    self = [super init];
    _layer = layer;
    _timeAccum = 0;
    _nextShotTime = 0;
    _currentShotIndex = 0;
    _sphere = sphere;
    _shots = [NSMutableArray new];
    _firePorts = [NSMutableArray new];
    // parse the data
    float firstShotDelay = ((NSNumber*)[shootingData objectForKey:@"firstShotDelay"]).floatValue;
    _nextShotTime = firstShotDelay;
    NSString *shootingMode = (NSString*)[shootingData objectForKey:@"mode"];
    NSAssert(shootingMode, @"No shooting mode defined!");
    if ([shootingMode isEqualToString:@"Projectile"])
    {
        NSArray *data = (NSArray*)[shootingData objectForKey:@"shots"];
        NSAssert(data, @"No shots data!");
        int i = 0;
        //        NSLog(@"%i",[data count]);
        while (i<[data count])
        {
            NSMutableArray *shot = [[[NSMutableArray alloc] init] autorelease];
            float timeDelay = 0;
            while (timeDelay == 0) {
                LSphereShootingProjectileData *projData = [[[LSphereShootingProjectileData alloc] init] autorelease];
                [self extractProjectileDataFromDict:(NSDictionary*)[data objectAtIndex:i] toSphereShootingProjectileData:projData];
                timeDelay = projData.delay;
                //                NSLog(@"%f",timeDelay);
                [shot addObject:projData];
                i++;
            }
            [_shots addObject:shot];
        }
        
        
        NSArray *storedFirePorts = (NSArray*)[shootingData objectForKey:@"firePorts"];
        NSAssert(storedFirePorts, @"No firePorts data!");
        for (NSString *str in storedFirePorts)
        {
            CGPoint p = CGPointFromString(str);
            [_firePorts addObject:[NSValue valueWithCGPoint:p]];
        }
        
    }
//    NSLog(@"Parsing shooting data for sphere ended with shots count:%i, firePorts count:%i",[_shots count], [_firePorts count]);
//    NSLog(@"retcount shot %i", [[_shots objectAtIndex:0] retainCount]);
//    NSLog(@"retcount projinshot %i", [[((NSArray*)[_shots objectAtIndex:0]) objectAtIndex:0] retainCount]);
    return self;
}

-(void)extractProjectileDataFromDict:(NSDictionary*)dict toSphereShootingProjectileData:(LSphereShootingProjectileData*)data
{
    data.angle = ((NSNumber*)[dict objectForKey:@"angle"]).floatValue;
    data.damage = ((NSNumber*)[dict objectForKey:@"damage"]).floatValue;
    data.delay = ((NSNumber*)[dict objectForKey:@"delay"]).floatValue;
    data.speedMod = ((NSNumber*)[dict objectForKey:@"speedMod"]).floatValue;
//    NSLog(@"%f s,m",data.speedMod);
    data.type = ((projectileType)((NSNumber*)[dict objectForKey:@"type"]).intValue);
    data.firePortNum = ((NSNumber*)[dict objectForKey:@"firePortNum"]).intValue;
}

-(void)onEnter
{
    [super onEnter];
    [self schedule:@selector(tick:)];
}



-(void)tick:(float)dt
{
    _timeAccum+=dt;
    if (_timeAccum > _nextShotTime)
    {
        _timeAccum = 0;
        
        NSMutableArray *shot = [_shots objectAtIndex:_currentShotIndex];
        
        for (LSphereShootingProjectileData *projData in shot)
        {
            if ([shot indexOfObject:projData] == [shot count]-1) // last projectile in shot
            {
                // take its delay as nextShotTime
                _nextShotTime = projData.delay;
            }
            
            // launch projectile
            CGPoint targetPos = [_sphere.target getTargetPosition];
//            NSLog(@"Target pos: %@",NSStringFromCGPoint(targetPos));
            targetPos = [self convertToNodeSpace:targetPos];
//            NSLog(@"Target pos SUB: %@",NSStringFromCGPoint(targetPos));
            float angle = ccpAngleSigned(ccp(0,1), targetPos);
//            if (targetPos.x<0) angle*=-1;
            angle += CC_DEGREES_TO_RADIANS(90);
            angle += CC_DEGREES_TO_RADIANS(projData.angle);
//            NSLog(@"ang %f",angle);
            Projectile *proj = [[[Projectile alloc] initWithType:projData.type andSpeedMod:projData.speedMod withAngle:angle onLayer:_layer] autorelease];
            [_layer addChild:proj];
            CGPoint fPort = [[_firePorts objectAtIndex:projData.firePortNum] CGPointValue];
            CGPoint fPortPos = ccpRotateByAngle(fPort, ccp(0,0), CC_DEGREES_TO_RADIANS(angle));
            proj.position = ccpAdd(_sphere.position, fPortPos);
            
            ProjectileActorInfo *projInfo = [[[ProjectileActorInfo alloc] initWithPosition:proj.position andRotation:0 andRect:CGRectMake(0, 0, proj.boundingBox.size.width, proj.boundingBox.size.height) andGotType:gotDynamic andAttType:attProjectile] autorelease];
            projInfo.hostObject = proj;
            projInfo.damage = projInfo.damage;
            [[SHb2dCollisionManager sharedSHb2dCollisionManager] addBoxBodyWithCircleShapeForGameObjectWithInfo:projInfo];
            proj.myInfo = projInfo;
            
        }
        _currentShotIndex += 1;
        if (_currentShotIndex > [_shots count]-1)
        {
            _currentShotIndex = 0;
        }

    }
//    if (_timeAccum > _player.currentWeapon.characteristics.firingRate)
//    {
//        _timeAccum = 0;
//        _projectileArmed = YES;
//    }
//    
//    if (_projectileArmed && _body.bodyJoystickIsTouched)
//    {
//        // shoot projectile
//        
//        Projectile *proj = [[[Projectile alloc] initWithType:_player.currentWeapon.characteristics.typeOfProjectile andSpeedMod:_player.currentWeapon.characteristics.speed withAngle:_body.shootingAngle onLayer:_layer] autorelease];
//
//        [_layer addChild:proj];
//        CGPoint fPort = [[_firePorts objectAtIndex:0] CGPointValue];
//        CGPoint firePortPos = ccpRotateByAngle(fPort, ccp(0,0), CC_DEGREES_TO_RADIANS(_body.shootingAngle));
//        proj.position = ccpAdd(_body.position, firePortPos);
//        
//        ProjectileActorInfo *projInfo = [[[ProjectileActorInfo alloc] initWithPosition:proj.position andRotation:0 andRect:CGRectMake(0, 0, _player.currentWeapon.characteristics.collisionRadius, _player.currentWeapon.characteristics.collisionRadius) andGotType:gotDynamic andAttType:attProjectile] autorelease];
//        projInfo.bounces = _player.currentWeapon.characteristics.bounces;
//        projInfo.penetration = _player.currentWeapon.characteristics.penetration;
//        projInfo.hostObject = _player;
//        projInfo.damage = _player.currentWeapon.characteristics.damage;
//        [[SHb2dCollisionManager sharedSHb2dCollisionManager] addBoxBodyWithCircleShapeForGameObjectWithInfo:projInfo];
//        proj.myInfo = projInfo;
//        //
//        
//        _projectileArmed = NO;
//        _timeAccum = 0;
//    }
}

-(void)dealloc
{
    [_shots release];
    [_firePorts release];
    [super dealloc];
}
@end
