//
//  HUDLayer.mm
//  Lightsaber
//
//  Created by Denis on 4/14/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "HUDLayer.h"
#import "LUIHandlePointPicker.h"
#import "GameplayLayer.h"

@implementation HUDLayer
@synthesize HPP = _HPP;
@synthesize gameplay = _gameplay;
-(id)init
{
    self = [super init];
    self.isTouchEnabled = YES;
    _HPP = nil;
    _HPPTouch = nil;
    _gameplay = nil;
    _resetLabel = [CCLabelTTF labelWithString:@"spawn" fontName:@"Marker Felt" fontSize:25];
    _resetMenuItemLabel = [CCMenuItemLabel itemWithLabel:_resetLabel target:self selector:@selector(resetButtonTouchInsideOut)];
    _sideMenu = [CCMenu menuWithItems:_resetMenuItemLabel, nil];
    [self addChild:_sideMenu];
    CGSize winSize = [CCDirector sharedDirector].winSize;
    _sideMenu.position = ccp(winSize.width*0.96,winSize.height*0.96);
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self scheduleUpdate];
}

-(void)addHandlePointPickerWithHandleSprite:(CCSprite *)handle andBeamSprite:(CCSprite *)beam andInfo:(SingleLightsaberActorInfo *)info
{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    self.HPP = [[[LUIHandlePointPicker alloc] initWithHandleSprite:handle andBeamSprite:beam andInfo:info] autorelease];
    
    [self addChild:self.HPP z:10];
    
    self.HPP.position = CGPointMake(-self.HPP.backgroundRect.size.width/2, winSize.height-self.HPP.backgroundRect.size.height);

//    NSLog(@"HPP POS %f,%f",self.HPP.position.x,self.HPP.position.y);
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
//    bool HPPgotTouch = NO;
//    for (UITouch *touch in touches)
//    {
//        if (touch != self.gameplay.firstTouchOnSaber && touch != self.gameplay.secondTouchOnSaber)
//        {
//            CGPoint location = [self convertTouchToNodeSpace:touch];
//            if (self.HPP != nil && _HPPTouch == nil)
//            {
//                HPPgotTouch = [self.HPP onTouchBegin:location];
//                if (HPPgotTouch)
//                {
//                    _HPPTouch = touch;
//                }
//                else {
//                    _HPPTouch = nil;
//                }
//            }
//        }
//    }

}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    bool HPPgotTouch = NO;
    for (UITouch *touch in touches)
    {
        if (touch != self.gameplay.firstTouchOnSaber && touch != self.gameplay.secondTouchOnSaber)
        {
            
            CGPoint location = [self convertTouchToNodeSpace:touch];
            if (self.HPP != nil && _HPPTouch == nil)
            {
                HPPgotTouch = [self.HPP onTouchBegin:location];
                if (HPPgotTouch)
                {
                    _HPPTouch = touch;
                }
                else {
                    _HPPTouch = nil;
                }
            }
            
            if (_HPPTouch != nil)
            {
                
                HPPgotTouch = [self.HPP onTouchMoved:location];
                
            }
        }
    }
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    bool HPPgotTouch = NO;
    for (UITouch *touch in touches)
    {
        if (touch != self.gameplay.firstTouchOnSaber && touch != self.gameplay.secondTouchOnSaber)
        {
            
            CGPoint location = [self convertTouchToNodeSpace:touch];
            if (self.HPP != nil && touch == _HPPTouch)
            {
                HPPgotTouch = [self.HPP onTouchEnded:location];
                
            }
        }
    }
    _HPPTouch = nil;
}

-(void)resetButtonTouchInsideOut
{
    [_gameplay testAddSphere];
}
     
-(void)update:(ccTime)dt
{
 
}

-(void)dealloc
{
    self.gameplay = nil;
    self.HPP = nil;
    [super dealloc];
}
@end
