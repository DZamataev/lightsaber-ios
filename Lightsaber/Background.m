//
//  Background.m
//  Lightsaber
//
//  Created by Denis on 4/14/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Background.h"


@implementation Background
@synthesize bg = _bg;
-(id)init
{
    self = [super init];
    _bg = [CCSprite spriteWithFile:@"Locations/Space.png"];
    _bg.anchorPoint = ccp(0,0);
    [self addChild:_bg z:0];
    return self;
}
@end
