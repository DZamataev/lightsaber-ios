//
//  LSphere.mm
//  Lightsaber
//
//  Created by Denis on 4/14/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "LSphere.h"
#import "LSphereActorInfo.h"
#import "SHb2dCollisionManager.h"
#import "LSphereShooting.h"
#import "LContactHolder.h"

@implementation LSphere
@synthesize myInfo = _myInfo;
@synthesize shootingScript = _shootingScript;
@synthesize sprite = _sprite;
@synthesize target = _target;
-(id)init
{
    NSAssert(NO, @"Original init unsupported"); 
    return self;
}

-(id)initWithType:(LSphereType)t andTarget:(id<LTargetProtocol>)targ
{
    self = [super init];
    _sprite = [CCSprite spriteWithFile:@"Spheres/test.png"];
    _sprite.scale = 0.7f;
    _sprite.opacity = 255;
    [self addChild:_sprite];
    _myInfo = nil;
    _shootingScript = nil;
    _type = t;
    _target = targ;
    _healthLabel = [CCLabelTTF labelWithString:@"" fontName:@"Marker Felt" fontSize:22];
    [self addChild:_healthLabel];
    _healthLabel.position = ccp(_healthLabel.position.x,_healthLabel.position.y+_sprite.boundingBox.size.height*1.1f);
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self setupInfoWithType:_type];
    [self setupPhysics];
    [self scheduleUpdate];
}

-(void)setupInfoWithType:(LSphereType)t
{
    self.myInfo = [[[LSphereActorInfo alloc] initWithPosition:self.position andRotation:0 andRect:_sprite.boundingBox andGotType:gotDynamic andAttType:attSphere] autorelease];
    NSString *typeString = [[NSString new] autorelease];
    
    // lets define the path to read all the data
    // 1) .toString() this fucking enum;
    switch (t) {
        case LSTSingle:
            typeString = @"Single";
            break;
            
        default:
            typeString = @"Single";
            break;
    }
    
    // reading the whole plist! Should be replaced by asking it elsewhere.
    NSString *plistFilename = NAME_OF_INFO_PLIST_FOR_SPHERES;
    
    NSError * error;
    NSArray * paths;
    NSString * documentsDirectory;
    NSString * path;
    NSFileManager * fileManager;
    NSString * bundle;
    
    NSString * fileName = plistFilename;
    NSString * fileType = @"plist";
    
    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    documentsDirectory = [paths objectAtIndex:0]; //2
    path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",fileName,fileType]]; //3
    [path retain];
    
    fileManager = [NSFileManager defaultManager];
    
#if OVERWRITE_EXISTING_INFO_PLIST_FOR_SPHERES == 1
    if (![fileManager fileExistsAtPath: path]) //4
    {
        bundle = [[NSBundle mainBundle] pathForResource:fileName ofType:fileType]; //5
        
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
    }
    else {
        // need overwriting
        bundle = [[NSBundle mainBundle] pathForResource:fileName ofType:fileType]; //5
        
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:bundle];
        [dict writeToFile:path atomically:YES];
    }
#else
    if (![fileManager fileExistsAtPath: path]) //4
    {
        bundle = [[NSBundle mainBundle] pathForResource:fileName ofType:fileType]; //5
        
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
        NSLog(@"%@",error);
    }
#endif
    
    NSMutableDictionary *infoDict = [[[NSMutableDictionary alloc] initWithContentsOfFile:path] autorelease];
    // reading end
    
    
    NSMutableDictionary *characteristicsDict = [[[NSMutableDictionary alloc] initWithDictionary:[infoDict objectForKey:typeString]] autorelease];
    
    NSAssert(characteristicsDict, @"Characteristics dict no found");
    
    _myInfo.collisionDamageMod = ((NSNumber*)[characteristicsDict objectForKey:@"collisionDamageMod"]).floatValue;
    _myInfo.burnDamageMod = ((NSNumber*)[characteristicsDict objectForKey:@"burnDamageMod"]).floatValue;
    _myInfo.projectileDamageMod = ((NSNumber*)[characteristicsDict objectForKey:@"projectileDamageMod"]).floatValue;
    _myInfo.collisionCirclePadding = ((NSNumber*)[characteristicsDict objectForKey:@"collisionCircleRadiusPercent"]).floatValue;
    _myInfo.health = ((NSNumber*)[characteristicsDict objectForKey:@"health"]).floatValue;
    _myInfo.mass = ((NSNumber*)[characteristicsDict objectForKey:@"mass"]).floatValue;
    _myInfo.linearDamping = ((NSNumber*)[characteristicsDict objectForKey:@"linearDamping"]).floatValue;
    _myInfo.angularDamping = ((NSNumber*)[characteristicsDict objectForKey:@"angularDamping"]).floatValue;
    _myInfo.colDensity = ((NSNumber*)[characteristicsDict objectForKey:@"colDensity"]).floatValue;
    _myInfo.colFriction = ((NSNumber*)[characteristicsDict objectForKey:@"colFriction"]).floatValue;
    _myInfo.colRestitution = ((NSNumber*)[characteristicsDict objectForKey:@"colRestitution"]).floatValue;
    
    [self setupShootingWithData:[NSMutableDictionary dictionaryWithDictionary:((NSDictionary*)[characteristicsDict objectForKey:@"shootingData"])]];
    [path release];
}

-(void)setupShootingWithData:(NSMutableDictionary *)shootingData
{
    NSAssert(shootingData, @"Missing shooting data!");
    _shootingScript = [[[LSphereShooting alloc] initWithSphere:self andLayer:((CCLayer*)self.parent) andShootingDataDict:shootingData] autorelease];
    [self addChild:_shootingScript];
    
}




-(void)setupPhysics
{
    NSAssert(_myInfo, @"Info is nil while trying to setup Physics");
    [[SHb2dCollisionManager sharedSHb2dCollisionManager] addSphereBodyWithInfo:_myInfo];
}

-(void)update:(ccTime)dt
{
    self.position = _myInfo.position;
    self.rotation = _myInfo.rotation;
//    NSLog(@"ps %@",NSStringFromCGPoint(self.position));
    
    // update contacts
    // projectile collision
    for (int i = 0; i<[_myInfo.projectileCollisionPool count];)
    {
        LContactHolder *h = [_myInfo.projectileCollisionPool objectAtIndex:i];
        h.lifeTimer -= dt;
        if (h.lifeTimer < 0)
        {
            [_myInfo.projectileCollisionPool removeObjectAtIndex:i];
        }
        else {
            i++;
        }
    }
    
    // saber direct collision
    if (_myInfo.burnContact)
    {
        _myInfo.burnContact = NO;
        _myInfo.health-=1*_myInfo.burnDamageMod;
    }
    if (_myInfo.beamCollisionDamage != 0)
    {
        _myInfo.health -= _myInfo.beamCollisionDamage*_myInfo.collisionDamageMod;
        _myInfo.beamCollisionDamage = 0;
    }
    _healthLabel.string = [NSString stringWithFormat:@"%.2f",_myInfo.health];
    if (_myInfo.health <= 0)
    {
        [self removeAllChildrenWithCleanup:YES];
        [self removeFromParentAndCleanup:YES];
    }
}

-(void)dealloc
{
    self.myInfo = nil;
    [super dealloc];
}
@end
