//
//  Projectile.h
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameActorInfo.h"
#import "ProjectileActorInfo.h"

@class SHProjectileTrail;
typedef enum {projectileFireBall = 1}projectileType;
@interface Projectile : CCNode {
    CCSprite *_sprite;
    projectileType _type;
    float _speed;
    CGPoint _move;
    ProjectileActorInfo *_myInfo;
    CGPoint _normalImpulse;
    bool _destroyed;
    CGPoint _previousPosition;
    bool _prevPosDefined;
    SHProjectileTrail *_trail;
    CCLayer *_layer;
    float _initialSpriteWidth;
    
}
@property projectileType type;
@property float speed;
@property (nonatomic, retain) ProjectileActorInfo *myInfo;
@property bool destroyed;
@property (nonatomic, retain) CCSprite *sprite;
//@property (nonatomic, retain) CCMotionStreak *mstreak;

-(id)initWithType:(projectileType)incType andSpeedMod:(float)incSpeedMod withAngle:(float)angle onLayer:(CCLayer*)layer;
-(void)onProjectileContactStateChange:(ProjectileContactState)toState;
-(void)update:(ccTime)dt;
-(void)destroy;
@end
