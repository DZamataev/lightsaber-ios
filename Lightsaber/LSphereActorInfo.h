//
//  LSphereActorInfo.h
//  Lightsaber
//
//  Created by Denis on 4/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameActorInfo.h"

typedef enum {LSTSingle=1}LSphereType;

@interface LSphereActorInfo : GameActorInfo
{
    LSphereType _type;
    float _health;
    float _collisionCirclePadding;
    float _burnDamageMod;
    float _collisionDamageMod;
    float _projectileDamageMod;
    float _mass;
    float _linearDamping;
    float _angularDamping;
    float _colDensity;
    float _colFriction;
    float _colRestitution;
    
    //contacts
    bool _burnContact;
    float _beamCollisionDamage;
    NSMutableArray *_projectileCollisionPool;
}
@property LSphereType type;
@property float health;
@property float collisionCirclePadding;
@property float burnDamageMod;
@property float projectileDamageMod;
@property float collisionDamageMod;
@property float mass;
@property float linearDamping;
@property float angularDamping;
@property float colDensity;
@property float colFriction;
@property float colRestitution;
@property bool burnContact;
@property float beamCollisionDamage;
@property (nonatomic, retain) NSMutableArray *projectileCollisionPool;

-(void)pushContactedObjectInfoProjectileCollisionPool:(GameActorInfo*)objInfo;
@end
