//
//  LPlume.m
//  Lightsaber
//
//  Created by Denis on 4/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LPlume.h"

@implementation LPlume
-(id)initWithBeamSize:(CGSize)bSize andCenterPoint:(CGPoint)cPoint andSaberNode:(CCNode*)sNode andMotionStreakInitData:(struct MotionStreakInitData)initData onLayer:(CCLayer *)layer withZIndex:(int)zed
{
    self = [super init];
    _didReset = NO;
    _motionStreaks = [NSMutableArray new];
    _motionStreaksPositions = [NSMutableArray new];
    _segments = [NSMutableArray new];
    zInd = zed;
    _layer = layer;
    _saberNode = sNode;
    float width = bSize.width;
    float height = bSize.height;
    CGPoint downPoint = ccp(cPoint.x,cPoint.y - (height/2));
    CGPoint topPoint = ccp(cPoint.x,cPoint.y + (height/2));
    float lenght = topPoint.y - downPoint.y;
    
    CGPoint firstStreakPos = ccp(topPoint.x,topPoint.y-width/2);
    CCMotionStreak *firstStreak = [CCMotionStreak streakWithFade:initData.fade minSeg:initData.minSeg width:initData.width color:initData.color texture:initData.texture];
    [_motionStreaks addObject:firstStreak];
    [_motionStreaksPositions addObject:[NSValue valueWithCGPoint:firstStreakPos]];
    firstStreak.position = firstStreakPos;
    lenght -= width/2;
    
    while (lenght>width) {
        lenght -= width;
        CGPoint currentStreakPos = CGPointMake(downPoint.x, downPoint.y+lenght);
        CCMotionStreak *streak = [CCMotionStreak streakWithFade:initData.fade minSeg:initData.minSeg width:initData.width color:initData.color texture:initData.texture];
        [_motionStreaks addObject:streak];
        [_motionStreaksPositions addObject:[NSValue valueWithCGPoint:currentStreakPos]];
        streak.position = currentStreakPos;
    }
    
    // here we should add the last streak - still nothing done
    
    //
    
    
    for (CCMotionStreak *streak in _motionStreaks)
    {
        [_layer addChild:streak z:zInd];
    }
    
    for (NSValue *val in _motionStreaksPositions)
    {
        CGPoint p = val.CGPointValue;
        [_segments addObject:[NSValue valueWithCGPoint:[_saberNode convertToNodeSpace:p]]];
    }
    
    return self;
}

-(void)updateStreaks
{
    if (_didReset)
    {
        [self resetStreaks];
    }
    for (int i = 0; i<[_motionStreaks count]; i++)
    {
        CCMotionStreak *streak = [_motionStreaks objectAtIndex:i];
        CGPoint segmentPos = ((NSValue*)[_segments objectAtIndex:i]).CGPointValue;
        streak.position = [_saberNode convertToWorldSpace:segmentPos];
    }
    if (_didReset)
    {
        [self resetStreaks];
        _didReset = NO;
    }
}

-(void)resetStreaks
{
    for (int i = 0; i<[_motionStreaks count]; i++)
    {
        CCMotionStreak *streak = [_motionStreaks objectAtIndex:i];
        [streak reset];
    }
    _didReset = YES;
}

-(void)interruptStreaks
{
    _didReset = YES;
}

-(void)dealloc
{
    for (CCMotionStreak *s in _motionStreaks)
    {
        [s removeFromParentAndCleanup:YES];
    }
    [_motionStreaks release];
    [_motionStreaksPositions release];
    [_segments release];
    [super dealloc];
}
@end
