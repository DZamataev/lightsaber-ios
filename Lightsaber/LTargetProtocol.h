//
//  LTargetProtocol.h
//  Lightsaber
//
//  Created by Denis on 4/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LTargetProtocol <NSObject>
-(CGPoint)getTargetPosition;
@end
