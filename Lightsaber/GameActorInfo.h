//
//  GameActorInfo.h
//  Shooter
//
//  Created by Denis on 3/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class SHBox2dBodyHolder;

typedef enum {gotStatic, gotDynamic} gameObjectType;
typedef enum {attSaber, attSaberBeam, attProjectile, attMapBounds, attSphere} attachedToType;


@interface GameActorInfo : NSObject
{
    CGPoint _position;
    CGFloat _rotation;
    CGRect _rect;
    gameObjectType _gotType;
    attachedToType _attType;
    
    id _hostObject;
    
    // box2d body representation held in wrapper
    SHBox2dBodyHolder *_b2dBodyHolder;
    
    // callbacks 
    bool _collisionDetected;
    bool _b2dContactDetected;
    
    CGPoint _b2bvelocity;
    CGPoint _b2impulse;
    
    CGPoint _forcedPosition;
    bool _forcedReposition;
    
    bool _physicsBodyRemoved;
}
@property CGPoint position;
@property CGFloat rotation;
@property CGRect rect;
@property gameObjectType gotType;
@property attachedToType attType;
@property (nonatomic, assign) id hostObject;

@property (nonatomic, assign) SHBox2dBodyHolder *b2dBodyHolder;

@property bool collisionDetected;
@property bool b2dContactDetected;

@property CGPoint b2bvelocity;
@property CGPoint b2impulse;

@property CGPoint forcedPosition;
@property bool forcedReposition;

-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att;
@end
