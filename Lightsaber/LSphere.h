//
//  LSphere.h
//  Lightsaber
//
//  Created by Denis on 4/14/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "LSphereActorInfo.h"
#import "LTargetProtocol.h"

#define NAME_OF_INFO_PLIST_FOR_SPHERES @"SphereCharacteristics"
#define OVERWRITE_EXISTING_INFO_PLIST_FOR_SPHERES 1

@class LSphereShooting;
@class LSphereShootingProjectileData;




@interface LSphere : CCNode {
    LSphereType _type;
    CCSprite *_sprite;
    LSphereActorInfo *_myInfo;
    LSphereShooting *_shootingScript;
    id <LTargetProtocol> _target;
    CCLabelTTF *_healthLabel;
}
@property (nonatomic, retain) LSphereActorInfo *myInfo;
@property (nonatomic, retain) LSphereShooting *shootingScript;
@property (nonatomic, retain) CCSprite *sprite;
@property (nonatomic, retain) id <LTargetProtocol> target;
-(id)initWithType:(LSphereType)t andTarget:(id<LTargetProtocol>)targ;
-(void)setupInfoWithType:(LSphereType)t;
-(void)setupShootingWithData:(NSMutableDictionary*)shootingData;
@end