//
//  SHb2dCollisionManager.mm
//  Shooter
//
//  Created by Denis on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHb2dCollisionManager.h"
#import "SingleLightsaberActorInfo.h"
#import "LSphereActorInfo.h"
#import "ProjectileActorInfo.h"
#import "SHBox2dBodyHolder.h"


@implementation SHb2dCollisionManager
@synthesize mapBoundsBody = _mapBoundsBody;
@synthesize saberBody = _saberBody;
@synthesize world = _world;
@synthesize saberWithHandleMassData = _saberWithHandleMassData;
CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(SHb2dCollisionManager)

-(id)init
{
    self = [super init];
    CGSize winSize = [CCDirector sharedDirector].winSize;
    b2Vec2 gravity = b2Vec2(0.0f, 0.0f);
    _world = new b2World(gravity);
    _world->SetAllowSleeping(false);
    _world->SetContinuousPhysics(true);
    
     // Create contact listener
    _contactListener = new MyContactListener();
    _world->SetContactListener(_contactListener);
    
    // Enable debug draw
    _debugDraw = new GLESDebugDraw( PTM_RATIO );
    _world->SetDebugDraw(_debugDraw);
    
    uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    _debugDraw->SetFlags(flags);
    
    _players = [[NSMutableArray alloc] init];
    [self createMapBounds:winSize];
    return self;
}


-(void) createMapBounds:(CGSize)size
{
    
	float width = size.width;
	float height = size.height;
    
	float32 margin = 1.0f;
	b2Vec2 lowerLeft = b2Vec2(margin/PTM_RATIO, margin/PTM_RATIO);
	b2Vec2 lowerRight = b2Vec2((width-margin)/PTM_RATIO,margin/PTM_RATIO);
	b2Vec2 upperRight = b2Vec2((width-margin)/PTM_RATIO, (height-margin)/PTM_RATIO);
	b2Vec2 upperLeft = b2Vec2(margin/PTM_RATIO, (height-margin)/PTM_RATIO);
    
    GameActorInfo *mapBoundsInfo = [[GameActorInfo alloc] initWithPosition:ccp(width/2,height/2) andRotation:0 andRect:CGRectMake(0, 0, width, height) andGotType:gotStatic andAttType:attMapBounds];
    
	b2BodyDef mapBoundsBodyDef;
	mapBoundsBodyDef.type = b2_staticBody;
    mapBoundsBodyDef.userData = mapBoundsInfo;
	mapBoundsBodyDef.position.Set(0, 0);
	_mapBoundsBody = _world->CreateBody(&mapBoundsBodyDef);
    
	// Define the ground box shape.
	b2EdgeShape mapBoundsBox;		
    b2FixtureDef mapBoundsFixtureDef;
    mapBoundsFixtureDef.shape = &mapBoundsBox;
    mapBoundsFixtureDef.filter.categoryBits = CATEGORY_BOUNDRY;
    mapBoundsFixtureDef.filter.maskBits = CATEGORY_SABER_HANDLE | CATEGORY_SPHERE | CATEGORU_PROJECTILE;
	// bottom
	mapBoundsBox.Set(lowerLeft, lowerRight);
    mapBoundsFixtureDef.shape = &mapBoundsBox;
	_mapBoundsBody->CreateFixture(&mapBoundsFixtureDef);
    
	// top
	mapBoundsBox.Set(upperRight, upperLeft);
    mapBoundsFixtureDef.shape = &mapBoundsBox;
	_mapBoundsBody->CreateFixture(&mapBoundsFixtureDef);
    
	// left
	mapBoundsBox.Set(upperLeft, lowerLeft);
    mapBoundsFixtureDef.shape = &mapBoundsBox;
	_mapBoundsBody->CreateFixture(&mapBoundsFixtureDef);
    
	// right
	mapBoundsBox.Set(lowerRight, upperRight);
    mapBoundsFixtureDef.shape = &mapBoundsBox;
	_mapBoundsBody->CreateFixture(&mapBoundsFixtureDef);
    
}


-(void)tick:(ccTime)dt
{
    // apply impulse
    
    for(b2Body *b = _world->GetBodyList(); b; b=b->GetNext()) {
        if (b->GetUserData() != NULL) {
            GameActorInfo *info = (GameActorInfo*)b->GetUserData();
            
            if (info.b2bvelocity.x!=0 || info.b2bvelocity.y != 0)
            {
                b2Vec2 imp = b2Vec2(info.b2bvelocity.x/PTM_RATIO, info.b2bvelocity.y/PTM_RATIO);
                
                b->SetLinearVelocity(imp);
                //b->ApplyLinearImpulse(imp, b->GetWorldCenter());
                
                info.b2bvelocity = CGPointZero;
            }
            
            if (info.b2impulse.x!=0 || info.b2impulse.y != 0)
            {
                b2Vec2 imp = b2Vec2(info.b2impulse.x/PTM_RATIO, info.b2impulse.y/PTM_RATIO);
                
                b->ApplyLinearImpulse(imp, b->GetWorldCenter());
                //b->ApplyLinearImpulse(imp, b->GetWorldCenter());
                
                info.b2impulse = CGPointZero;
                
                if (info.attType = attProjectile)
                {
                    ProjectileActorInfo *projInfo = (ProjectileActorInfo*)info;
                    if (projInfo.constantSpeed)
                    {
                        b2Vec2 vel = b->GetLinearVelocity();
                        float speed = vel.Length();
                        projInfo.maximumSpeed = speed;
                    }
                }
            }
            
        }
    }
    // do step
    _world->Step(dt, 2, 1);
    
    // find velocities to limit
    for(b2Body *b = _world->GetBodyList(); b; b=b->GetNext()) {
        if (b->GetUserData() != NULL) {
            GameActorInfo *info = (GameActorInfo*)b->GetUserData();
            if (info.attType == attProjectile)
            {
                ProjectileActorInfo *projInfo = (ProjectileActorInfo*)info;
                if (projInfo.constantSpeed)
                {
                    b2Vec2 vel = b->GetLinearVelocity();
                    float speed = vel.Length();
                    if (speed <= projInfo.maximumSpeed)
                    {
                        b->SetLinearVelocity((projInfo.maximumSpeed / speed) * vel);
                    }
                }
            }

        }
    }
    
    // update positions for cocos2d via their gameActorInfo
    for(b2Body *b = _world->GetBodyList(); b; b=b->GetNext()) {
        if (b->GetUserData() != NULL) {
            GameActorInfo *info = (GameActorInfo*)b->GetUserData();
                                 
            info.position = ccp(b->GetPosition().x * PTM_RATIO,
                                  b->GetPosition().y * PTM_RATIO);
            info.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
        }
    }
    
    // lookup for contacts in contact listener, attached to the world
    std::vector<MyContact>::iterator pos;
    for(pos = _contactListener->_contacts.begin(); 
        pos != _contactListener->_contacts.end(); ++pos) {
        MyContact contact = *pos;
        
        b2Body *bodyA = contact.fixtureA->GetBody();
        b2Body *bodyB = contact.fixtureB->GetBody();
        if (bodyA->GetUserData() != NULL && bodyB->GetUserData() != NULL) {
            GameActorInfo *infoA = (GameActorInfo*) bodyA->GetUserData();
            GameActorInfo *infoB = (GameActorInfo*) bodyB->GetUserData();
            
            [self manageContactWithActorInfo:infoA andActorInfo:infoB];
        }        
    }
    
    // callbacks
    
}

-(void)drawDebug
{
    return;
	//
	// IMPORTANT:
	// This is only for debug purposes
	// It is recommend to disable it
	//
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	_world->DrawDebugData();	
	
	kmGLPopMatrix();
}

#pragma mark -
#pragma mark - Add box2d body methods

-(void)addBoxBodyWithCircleShapeForGameObjectWithInfo:(GameActorInfo *)info
{
    b2BodyDef spriteBodyDef;
    SHBodyInitType initParams = [self switchInitTypeWithGOT:info.gotType andATT:info.attType];
    spriteBodyDef.type = initParams.btype;
    spriteBodyDef.linearDamping = initParams.linearDamping;
    spriteBodyDef.angularDamping = initParams.angularDamping;
    spriteBodyDef.bullet = initParams.isBullet;

    spriteBodyDef.position.Set(info.position.x/PTM_RATIO, 
                               info.position.y/PTM_RATIO);
    spriteBodyDef.userData = info;
    b2Body *spriteBody = _world->CreateBody(&spriteBodyDef);
    
    if (initParams.isPlayer)
    {
        [_players addObject:info];

        if (info.attType == attSaber)
        {
            _saberBody = spriteBody;
        }
    }
    
    // output the body for info into the wrapper
    info.b2dBodyHolder.theB2Body = spriteBody;
    info.b2dBodyHolder.theWorld = _world;
    
    b2CircleShape spriteShape;
    float circleRadius;
    if (info.rect.size.width>info.rect.size.height)
        circleRadius = info.rect.size.width/2/PTM_RATIO;
    else
        circleRadius = info.rect.size.height/2/PTM_RATIO;
    spriteShape.m_radius = circleRadius;
    
    b2FixtureDef spriteShapeDef;
    spriteShapeDef.shape = &spriteShape;
    spriteShapeDef.density = 10.0;
    spriteShapeDef.friction = initParams.friction;
    spriteShapeDef.restitution = initParams.restitution;
    spriteShapeDef.isSensor = initParams.isSensor;
    spriteShapeDef.filter.categoryBits = initParams.categoryBits;
    spriteShapeDef.filter.maskBits = initParams.maskBits;
    spriteBody->CreateFixture(&spriteShapeDef);
    
    if (initParams.customMass)
    {
        // take mass data
        struct b2MassData spriteMassData;
        spriteBody->GetMassData(&spriteMassData);
        //edit mass data
        spriteMassData.mass = initParams.mass;
        spriteBody->SetMassData(&spriteMassData);
    }
}

-(void)addBoxBodyWithPolygonShapeForGameObjectWithInfo:(GameActorInfo *)info
{
    b2BodyDef spriteBodyDef;
    SHBodyInitType initParams = [self switchInitTypeWithGOT:info.gotType andATT:info.attType];
    spriteBodyDef.type = initParams.btype;
    spriteBodyDef.linearDamping = initParams.linearDamping;
    spriteBodyDef.angularDamping = initParams.angularDamping;
    spriteBodyDef.bullet = initParams.isBullet;
    spriteBodyDef.position.Set(info.position.x/PTM_RATIO, 
                               info.position.y/PTM_RATIO);
    spriteBodyDef.userData = info;
    b2Body *spriteBody = _world->CreateBody(&spriteBodyDef);
    if (initParams.isPlayer)
    {
        [_players addObject:info];
        if (info.attType == attSaber)
        {
            _saberBody = spriteBody;
        }
    }
    
    
    // output the body for info into the wrapper
    info.b2dBodyHolder.theB2Body = spriteBody;
    info.b2dBodyHolder.theWorld = _world;
    
    b2PolygonShape spriteShape;
    spriteShape.SetAsBox(info.rect.size.width/PTM_RATIO/2,
                         info.rect.size.height/PTM_RATIO/2);
    
    b2FixtureDef spriteShapeDef;
    spriteShapeDef.shape = &spriteShape;
    spriteShapeDef.density = 10.0;
    
    spriteShapeDef.friction = initParams.friction;
    spriteShapeDef.restitution = initParams.restitution;
    spriteShapeDef.isSensor = initParams.isSensor;

    spriteBody->CreateFixture(&spriteShapeDef);
}

-(void)addSingleLightsaberBodyWithInfo:(GameActorInfo*)info
{
    NSAssert([info isKindOfClass:[SingleLightsaberActorInfo class]], @"Wrong class!");
    b2BodyDef spriteBodyDef;
    SingleLightsaberActorInfo *saberInfo = (SingleLightsaberActorInfo*)info;
    [_players addObject:info];
    
    spriteBodyDef.type = b2_dynamicBody;
    spriteBodyDef.linearDamping = 10.0f;
    spriteBodyDef.angularDamping = 9.0f;
    spriteBodyDef.bullet = TRUE;
    
    spriteBodyDef.position.Set(saberInfo.position.x/PTM_RATIO, 
                               saberInfo.position.y/PTM_RATIO);
    spriteBodyDef.userData = info;
    b2Body *spriteBody = _world->CreateBody(&spriteBodyDef);
    _saberBody = spriteBody;
    
    // output the body for info into the wrapper
    info.b2dBodyHolder.theB2Body = spriteBody;
    info.b2dBodyHolder.theWorld = _world;
    
    b2PolygonShape handleShape;
    handleShape.SetAsBox(info.rect.size.width/PTM_RATIO/2,
                         info.rect.size.height/PTM_RATIO/2);
    
    b2FixtureDef handleShapeDef;
    handleShapeDef.filter.categoryBits = CATEGORY_SABER_HANDLE;
    handleShapeDef.filter.maskBits = CATEGORY_BOUNDRY | CATEGORY_SPHERE | CATEGORU_PROJECTILE;
    handleShapeDef.shape = &handleShape;
    
    handleShapeDef.density = 10.0;
    handleShapeDef.friction = 0.0f;
    handleShapeDef.restitution = 0.0f;
    handleShapeDef.isSensor = false;
    
    spriteBody->CreateFixture(&handleShapeDef);
    
    
    // write the mass
    b2MassData copyOfMData;
    spriteBody->GetMassData(&copyOfMData);
    _saberWithHandleMassData = copyOfMData;
    
    // beam
    
    b2PolygonShape beamShape;
    //handle h, beam b
    float hh=saberInfo.rect.size.height;
    float bw=saberInfo.beamRect.size.width;
    float bh=saberInfo.beamRect.size.height;
    
	b2Vec2 lowerLeft = b2Vec2(-bw/2/PTM_RATIO, (+hh/2)/PTM_RATIO);
	b2Vec2 lowerRight = b2Vec2((bw/2)/PTM_RATIO,(+hh/2)/PTM_RATIO);
	b2Vec2 upperRight = b2Vec2((bw/2)/PTM_RATIO, ((bh)+hh/2)/PTM_RATIO);
	b2Vec2 upperLeft = b2Vec2(-bw/2/PTM_RATIO, ((bh)+hh/2)/PTM_RATIO);
    
    b2Vec2 vertices[4];
    int32 count = 4;
    vertices[0] = lowerLeft;
    vertices[1] = lowerRight;
    vertices[2] = upperRight;
    vertices[3] = upperLeft;
    
    beamShape.Set(vertices, count);
    
    b2FixtureDef beamShapeDef;
    beamShapeDef.filter.categoryBits = CATEGORY_SABER_BEAM;
    beamShapeDef.filter.maskBits = CATEGORY_SPHERE | CATEGORU_PROJECTILE | CATEGORY_SPHERE_SENSOR;
    beamShapeDef.shape = &beamShape;
    
    beamShapeDef.density = 10.0;
    beamShapeDef.friction = 0.0f;
    beamShapeDef.restitution = 0.0f;
    beamShapeDef.isSensor = false;
    
    spriteBody->CreateFixture(&beamShapeDef);
}

-(void)addSphereBodyWithInfo:(GameActorInfo*)info
{
    NSAssert([info isKindOfClass:[LSphereActorInfo class]], @"Wrong class!");
    LSphereActorInfo *sphereInfo = (LSphereActorInfo*)info;
    b2BodyDef spriteBodyDef;
    spriteBodyDef.type = b2_dynamicBody;
    spriteBodyDef.linearDamping = sphereInfo.linearDamping;
    spriteBodyDef.angularDamping = sphereInfo.angularDamping;
    spriteBodyDef.bullet = true;
    
    spriteBodyDef.position.Set(info.position.x/PTM_RATIO, 
                               info.position.y/PTM_RATIO);
    spriteBodyDef.userData = info;
    b2Body *spriteBody = _world->CreateBody(&spriteBodyDef);
    
    
    // output the body for info into the wrapper
    info.b2dBodyHolder.theB2Body = spriteBody;
    info.b2dBodyHolder.theWorld = _world;
    
    b2CircleShape collisionShape;
    float collisionCircleRadius;
    if (info.rect.size.width>info.rect.size.height)
        collisionCircleRadius = info.rect.size.width/2/PTM_RATIO;
    else
        collisionCircleRadius = info.rect.size.height/2/PTM_RATIO;
    
    collisionShape.m_radius = collisionCircleRadius-sphereInfo.collisionCirclePadding*collisionCircleRadius;
    b2FixtureDef collisionShapeDef;
    collisionShapeDef.shape = &collisionShape;
    collisionShapeDef.density = sphereInfo.colDensity;
    collisionShapeDef.friction = sphereInfo.colFriction;
    collisionShapeDef.restitution = sphereInfo.colRestitution;
    collisionShapeDef.isSensor = false;
    collisionShapeDef.filter.categoryBits = CATEGORY_SPHERE;
    collisionShapeDef.filter.maskBits = CATEGORY_SPHERE | CATEGORY_BOUNDRY | CATEGORY_SABER_BEAM | CATEGORU_PROJECTILE;
    
    spriteBody->CreateFixture(&collisionShapeDef);
    
    // take mass data
    struct b2MassData colMassData;
    spriteBody->GetMassData(&colMassData);
    
    b2CircleShape spriteShape;
    float circleRadius;
    if (info.rect.size.width>info.rect.size.height)
        circleRadius = info.rect.size.width/2/PTM_RATIO;
    else
        circleRadius = info.rect.size.height/2/PTM_RATIO;
    spriteShape.m_radius = circleRadius;
    
    b2FixtureDef spriteShapeDef;
    spriteShapeDef.shape = &spriteShape;
    spriteShapeDef.density = 0.0f;
    spriteShapeDef.friction = 0.0f;
    spriteShapeDef.restitution = 0.0f;
    spriteShapeDef.isSensor = false;
    spriteShapeDef.filter.categoryBits = CATEGORY_SPHERE_SENSOR;
    spriteShapeDef.filter.maskBits = CATEGORY_SABER_BEAM;
    spriteBody->CreateFixture(&spriteShapeDef);

    //edit mass data
    colMassData.mass = sphereInfo.mass;
    spriteBody->SetMassData(&colMassData);
}
-(SHBodyInitType)switchInitTypeWithGOT:(gameObjectType)got andATT:(attachedToType)att
{
    SHBodyInitType initType;
    switch (got) {
        case gotStatic:
            switch (att) {
                    
                default:
                    NSAssert(NO, @"### WARNING! switchInitType got unrecognized attachedToType.");
                    break;
            }
            
            break;
            
        case gotDynamic:
            switch (att) {
                    
                case attProjectile:
                    initType.btype = b2_dynamicBody;
                    initType.isSensor = false;
                    initType.isPlayer = false;
                    initType.linearDamping = 0.1f;
                    initType.angularDamping = 0.0f;
                    initType.friction = 0.0f;
                    initType.restitution = 1.0f;
                    initType.isBullet = NO;
                    initType.categoryBits = CATEGORU_PROJECTILE;
                    initType.maskBits = CATEGORY_BOUNDRY | CATEGORY_SABER_BEAM | CATEGORY_SABER_HANDLE;
                    initType.customMass = YES;
                    initType.mass = 0.001f;
                    break;
                    
                    
                default:
                    NSAssert(NO, @"### WARNING! switchInitType got unrecognized attachedToType.");
                    break;
            }
            
            break;
            
            
        default:
            NSAssert(NO, @"### WARNING! switchInitType got unrecognized gameObjectType.");
            break;
    }
    return initType;
}

#pragma mark Manage Contact

-(void)manageContactWithActorInfo:(GameActorInfo *)infoA andActorInfo:(GameActorInfo *)infoB
{
    if (infoA.gotType == gotDynamic && infoB.gotType == gotStatic) //
    {
        [self manageContactWithDynamicActorInfo:infoA andStaticActorInfo:infoB];
    }
    else if (infoA.gotType == gotStatic && infoB.gotType == gotDynamic)
    {
        [self manageContactWithDynamicActorInfo:infoB andStaticActorInfo:infoA];
    }
    else if (infoA.gotType == gotDynamic && infoB.gotType == gotDynamic)
    {
        [self manageContactWithDynamicActorInfo:infoA andDynamicActorInfo:infoB];
    }
    else if (infoA.gotType == gotStatic && infoB.gotType == gotStatic)
    {
        
    }
}

-(void)manageContactWithDynamicActorInfo:(GameActorInfo *)dynInfo 
                      andStaticActorInfo:(GameActorInfo *)staticInfo
{
    
    if (dynInfo.attType == attSaber)
    {

    }
    // projectile vs map border collision
    else if (dynInfo.attType == attProjectile)
    {
        if (staticInfo.attType == attMapBounds)
        {
            ProjectileActorInfo *projInfo = (ProjectileActorInfo*)dynInfo;
            if (projInfo.contactState == pcsSaber)
            {
                projInfo.b2dContactDetected = YES;
            }
        }
    }
 
}

-(void)manageContactWithDynamicActorInfo:(GameActorInfo *)dynInfoA 
                     andDynamicActorInfo:(GameActorInfo *)dynInfoB
{

}

#pragma mark -
#pragma mark Non box2d collision tests

-(bool)rect:(CGRect)rect intersectsCircleWithOrigin:(CGPoint)p1 andRadius:(CGFloat)r
{
    CGPoint circleDistance;
    circleDistance.x = fabsf(p1.x - rect.origin.x - rect.size.width/2);
    circleDistance.y = fabsf(p1.y - rect.origin.y - rect.size.height/2);
    
    if (circleDistance.x > (rect.size.width/2 + r)) { return false; }
    if (circleDistance.y > (rect.size.height/2 + r)) { return false; }
    
    if (circleDistance.x <= (rect.size.width/2)) { return true; } 
    if (circleDistance.y <= (rect.size.height/2)) { return true; }
    
    float cornerDistance_sq = powf((circleDistance.x - rect.size.width/2),2) +
    powf((circleDistance.y - rect.size.height/2),2);
    
    return (cornerDistance_sq <= powf(r, 2));
}



-(CGRect)calculateRectForPosition:(CGPoint)position andSize:(CGSize)size
{
    return CGRectMake(position.x-size.width/2,
                      position.y-size.height/2,
                      size.width,
                      size.height);
}

-(bool)rectCollisionTestForActorWithInfo:(GameActorInfo *)info1 andActorWithInfo:(GameActorInfo *)info2
{
    return CGRectIntersectsRect(info1.rect, info2.rect);
}
//-(void)addStaticBoxBodyWithPolygonShapeForRect:(CGRect)rect
//{
//    b2BodyDef spriteBodyDef;
//    spriteBodyDef.type = b2_staticBody;
//    spriteBodyDef.position.Set((rect.origin.x+rect.size.width/2)/PTM_RATIO, 
//                               (rect.origin.y+rect.size.height/2)/PTM_RATIO);
//    //spriteBodyDef.userData = nil;
//    b2Body *spriteBody = _world->CreateBody(&spriteBodyDef);
//    
//    b2PolygonShape spriteShape;
//    spriteShape.SetAsBox(rect.size.width/PTM_RATIO/2,
//                         rect.size.height/PTM_RATIO/2);
//    
//    b2FixtureDef spriteShapeDef;
//    spriteShapeDef.shape = &spriteShape;
//    spriteShapeDef.density = 10.0;
//    spriteShapeDef.isSensor = true;
//    spriteBody->CreateFixture(&spriteShapeDef);
//}
@end
