//
//  LContactHolder.h
//  Lightsaber
//
//  Created by Denis on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GameActorInfo;

@interface LContactHolder : NSObject
{
    GameActorInfo *_contactedObjectInfo;
    float _lifeTimer;
}
@property float lifeTimer;
@property (nonatomic, retain) GameActorInfo *contactedObjectInfo;
+(LContactHolder*)LCHolderWithContactedObjectInfo:(GameActorInfo*)objInfo;

@end
